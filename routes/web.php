<?php

use App\Http\Controllers\CallsController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\AdminMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to('calls');
});

Route::group(['middleware' => 'auth'], function() {

//Отчеты по пропущенным звонкам
    Route::get('calls/{date?}', [CallsController::class, 'index'])
        ->name('calls.index')
        ->where('date', '202[0-9]-[0-9]{2}-[0-9]{2}');
    Route::post('calls', [CallsController::class, 'store'])
        ->name('calls.update');
    Route::put('calls', [CallsController::class, 'updateCallsStatus'])
        ->name('calls.status.update');

//Личный кабинет
    Route::get('personal', [UserController::class, 'personalPage'])
        ->name('users.personal');
    Route::get('password-reset', [UserController::class, 'passwordResetFrom'])
        ->name('users.password-reset');
    Route::post('password-reset', [UserController::class, 'passwordResetHandler'])
        ->name('users.password-reset-handler');

//Админка
    Route::group(['middleware' => [AdminMiddleware::class]], function () {
        Route::get('users', [UserController::class, 'index'])
            ->name('admin.users.index');
        Route::get('users/create', [UserController::class, 'create'])
            ->name('admin.users.create');
        Route::post('users', [UserController::class, 'store'])
            ->name('admin.users.store');
        Route::get('users/{id}/edit', [UserController::class, 'edit'])
            ->name('admin.users.edit');
        Route::delete('users/{id}', [UserController::class, 'destroy'])
            ->name('admin.users.destroy');
        Route::put('users/{id}', [UserController::class, 'update'])
            ->name('users.update');
    });
});

Auth::routes(['register' => false]);
