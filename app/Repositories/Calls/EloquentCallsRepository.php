<?php


namespace App\Repositories\Calls;


use App\Models\Call;
use Illuminate\Pagination\LengthAwarePaginator;

class EloquentCallsRepository implements CallsRepositoryInterface
{

    public function createMany(array $data)
    {
        return Call::query()->insertOrIgnore($data);
    }

    /**
     * @param $date
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getByDate($date): LengthAwarePaginator
    {
        return Call::query()
            ->whereDate('date_time', '=', $date)
            ->orderBy('date_time', 'desc')
            ->paginate(20);
    }

    /**
     * @param string $number
     * @param string $date
     * @param bool $status
     * @return int
     */
    public function changeStatusByNumberAndDate(string $number, string $date, bool $status)
    {
        return Call::query()
            ->where('from_phone_number', $number)
            ->whereDate('date_time', '=', $date)
            ->update(['processed' => $status]);
    }
}
