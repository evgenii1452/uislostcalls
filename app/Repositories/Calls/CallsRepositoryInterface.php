<?php


namespace App\Repositories\Calls;


use Illuminate\Pagination\LengthAwarePaginator;

interface CallsRepositoryInterface
{
    public function createMany(array $data);

    public function getByDate($date): LengthAwarePaginator;

    public function changeStatusByNumberAndDate(string $number, string $date, bool $status);
}
