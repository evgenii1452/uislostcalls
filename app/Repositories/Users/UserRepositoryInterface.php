<?php


namespace App\Repositories\Users;


interface UserRepositoryInterface
{
    public function store(array $data);

    public function getAll();

    public function destroy(int $id);

    public function getById(int $id);

    public function updateById(int $id, array $data);

}
