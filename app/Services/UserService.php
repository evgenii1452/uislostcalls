<?php


namespace App\Services;


use App\Models\User;
use App\Repositories\Users\EloquentUserRepository;
use App\Repositories\Users\UserRepositoryInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * @var EloquentUserRepository
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $data
     * @return User|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function createUser(array $data): User
    {
        $data['password'] = Hash::make($data['password']);

        return $this->userRepository->store($data);
    }


    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllUsers(): Collection
    {
        return $this->userRepository->getAll();
    }


    /**
     * @param int $id
     * @return int
     */
    public function deleteUser(int $id): int
    {
        $user = $this->getUserById($id);

        if ($user->isAdmin()) {
            abort(403, 'Невозможно удалить админитратора');
        }

        return $this->userRepository->destroy($id);
    }

    /**
     * @param int $id
     * @return User|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getUserById(int $id): User
    {
        $user = $this->userRepository->getById($id);

        return $user;
    }

    /**
     * @param int $id
     * @param array $data
     * @return int
     */
    public function updateUserById(int $id, array $data): int
    {
        foreach ($data as $key => $value) {

            if ($value == '') {
                unset($data[$key]);
            }

        }

        if (isset($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        return $this->userRepository->updateById($id, $data);
    }

    /**
     * @param array $data
     * @param User $user
     * @return bool
     */
    public function passwordReset(array $data, Authenticatable $user): bool
    {
        if (!Hash::check($data['old_password'], $user->password)){
            return false;
        }

        $this->userRepository->updateById(
            $user->id,
            ['password' => Hash::make($data['password'])]
        );

        return true;
    }
}
