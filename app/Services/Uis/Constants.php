<?php


namespace App\Services\Uis;


class Constants
{
    /* Поставщики API */
    const UIS = 'UIS';

    const CALL_GEAR = 'CallGear';

    /* Названия API сервисов */
    const CALL_API_SERVICE = 'CallApi';

    const DATA_API_SERVICE = 'DataApi';

    /* URL адреса API сервисов UIS */
    const UIS_CALL_API_HOST = 'https://callapi.uiscom.ru';

    const UIS_DATA_API_HOST = 'https://dataapi.uiscom.ru';

    const CALL_GEAR_CALL_API_HOST = 'https://callapi.callgear.com';

    const CALL_GEAR_DATA_API_HOST = 'https://dataapi.callgear.com';

    const API_HOSTS = [
        self::UIS => [
            self::CALL_API_SERVICE => self::UIS_CALL_API_HOST,
            self::DATA_API_SERVICE => self::UIS_DATA_API_HOST,
        ],
        self::CALL_GEAR => [
            self::CALL_API_SERVICE => self::CALL_GEAR_CALL_API_HOST,
            self::DATA_API_SERVICE => self::CALL_GEAR_DATA_API_HOST,
        ],
    ];

    /* Обозначение посетителя и оператора в терминологии UIS */
    const SITE_VISITOR = 'contact';

    const CLIENT_OPERATOR = 'operator';

    /* Варианты авторизации в API */
    const AUTH_BY_CREDENTIALS = 1;

    const AUTH_BY_API_KEY = 2;

    /* Версии API */
    const CALL_API_VERSION_4 = 'v4.0';

    const DATA_API_VERSION_2 = 'v2.0';

    /* Опции HTTP-запроса по умолчанию */
    const USER_AGENT = 'CBH-Uiscom-Client';

    /* Названия конечных методов API */
    const CALL_API_START_SIMPLE_CALL_RESOURCE = 'start.simple_call';

    const CALL_API_START_MULTI_CALL_RESOURCE = 'start.multi_call';

    const CALL_API_TAG_CALL_RESOURCE = 'tag.call';

    const DATA_API_GET_ACCOUNT_RESOURCE = 'get.account';

    const DATA_API_GET_CALLS_REPORT_RESOURCE = 'get.calls_report';

    const DATA_API_GET_FIN_CALL_LEG_REPORT_RESOURCE = 'get.financial_call_legs_report';

    const DATA_API_SET_COMMUNICATION_TAG_RESOURCE = 'set.tag_communications';

    /* Названия методов ресурсов */
    const CALL_API_START_SIMPLE_CALL_METHOD = 'getSimpleCall';

    const CALL_API_START_MULTI_CALL_METHOD = 'getMultiCall';

    const CALL_API_TAG_CALL_METHOD = 'setCallTag';

    const DATA_API_GET_ACCOUNT_METHOD = 'getAccount';

    const DATA_API_GET_CALLS_REPORT_METHOD = 'getCallsReport';

    const DATA_API_GET_FIN_CALL_LEGS_REPORT_METHOD = 'getFinancialCallLegsReport';

    const DATA_API_SET_COMMUNICATION_TAG_METHOD = 'setCommunicationTag';

    /* Типы медиа, проигрываемых каждому плечу */
    const MESSAGE_TYPE_TTS = 'tts';

    const MESSAGE_TYPE_MEDIA = 'media';

    /* Типы обращений */
    const COMMUNICATION_TYPE_CHAT = 'chat';

    const COMMUNICATION_TYPE_CALL = 'call';

    const COMMUNICATION_TYPE_GOAL = 'goal';

    const COMMUNICATION_TYPE_OFFLINE_MESSAGE = 'offline_message';

    /* Источники звонка  */
    const CALL_SOURCE_CALL_API = 'callapi';
}
