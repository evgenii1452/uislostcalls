<?php


namespace App\Services;


use App\Repositories\Calls\CallsRepositoryInterface;
use App\Services\Uis\UisApiService;
use Carbon\Carbon;

class CallsService
{
    /**
     * @var CallsRepositoryInterface
     */
    private $callsRepository;
    /**
     * @var UisApiService
     */
    private $uisApiService;


    /**
     * CallsService constructor.
     * @param CallsRepositoryInterface $callsRepository
     * @param UisApiService $uisApiService
     */
    public function __construct(
        CallsRepositoryInterface $callsRepository,
        UisApiService $uisApiService
    )
    {
        $this->callsRepository = $callsRepository;
        $this->uisApiService = $uisApiService;
    }

    public function requestCallsReportFromUisApiByDate($date)
    {
        $date = $this->getDateIfCorrectFormat($date);
        $callsReport = $this->uisApiService->getCallsReportByDate($date);

        return $callsReport;
    }

    /**
     * @param $callsReport
     * @return mixed
     */
    public function storeFromCallsReport($callsReport)
    {
        $data = [];

        foreach ($callsReport->data as $call) {
            $data[] = [
                'id' => $call->id,
                'is_lost' => $call->is_lost,
                'date_time' => $call->start_time,
                'finish_reason' => $call->finish_reason,
                'region_name' => $call->cpn_region_name,
                'from_phone_number' => $call->contact_phone_number,
                'to_phone_number' => $call->virtual_phone_number,
                'created_at' => Carbon::now(),
            ];
        }

        return $this->callsRepository->createMany($data);
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getCallsByDate(string $date)
    {

        $date = $this->getDateIfCorrectFormat($date);

        $callsPaginator = $this->callsRepository->getByDate($date);

        return $callsPaginator;
    }

    /**
     * @param string $date
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function storeCallsByDate(string $date)
    {
        $date = $this->getDateIfCorrectFormat($date);

        $callsReport = $this->requestCallsReportFromUisApiByDate($date);
        return $this->storeFromCallsReport($callsReport);
    }

    /**
     * @param string $number
     * @param string $date
     * @param bool $status
     * @return mixed
     */
    public function changeStatusCallsByNumberAndDate(string $number, string $date, bool $status)
    {
        $date = Carbon::createFromDate($date)->format("Y-m-d");

        return $this->callsRepository->changeStatusByNumberAndDate($number, $date, $status);
    }

    /**
     * @param string $date
     * @return string
     */
    private function getDateIfCorrectFormat(string $date)
    {
        if (!Carbon::hasFormat($date, "Y-m-d")) {
            return null;
        }

        return $date;
    }

}
