<?php


namespace App\Policies;


class Abilities
{
    const INDEX = 'index';
    const CREATE = 'create';
    const STORE = 'store';
    const UPDATE = 'update';
    const EDIT = 'edit';
    const DELETE = 'delete';
}
