<?php


namespace App\Http\Controllers;


use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Policies\Abilities;
use App\Services\UserService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function personalPage()
    {
        $user = Auth::user();

        return view('users.personal-page', compact('user'));
    }

    /**
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize(Abilities::INDEX, Auth::user());

        $users = $this->userService->getAllUsers();

        return view('users.admin.user-list', compact('users'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(int $id)
    {
        $this->authorize(Abilities::DELETE, Auth::user());

        $this->userService->deleteUser($id);

        return redirect()->back();
    }

    public function create()
    {
        return view('users.admin.user-create');
    }

    /**
     * @param UserCreateRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(UserCreateRequest $request)
    {
        $this->authorize(Abilities::STORE, Auth::user());
        $data = $request->only(['name', 'role', 'password', 'email', 'login']);


        $this->userService->createUser($data);

        return redirect()->route("admin.users.index");
    }

    /**
     * @param int $id
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $user = $this->userService->getUserById($id);

        $this->authorize(Abilities::EDIT, [Auth::user(), $user]);

        return view('users.user-edit', compact('user'));
    }

    /**
     * @param int $id
     * @param UserUpdateRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(int $id, UserUpdateRequest $request)
    {
        $this->authorize(Abilities::UPDATE, [Auth::user(), $this->userService->getUserById($id)]);

        $data = $request->only(['name', 'role', 'password', 'email']);

        $this->userService->updateUserById($id, $data);

        return redirect()->route("admin.users.index");
    }

    /**
     * @return Factory|View
     */
    public function passwordResetFrom()
    {
        $user = Auth::user();

        return view('users.password-reset', compact('user'));
    }

    /**
     * @param PasswordResetRequest $request
     * @return Factory|View
     */
    public function passwordResetHandler(PasswordResetRequest $request)
    {
        $user = Auth::user();
        $data = $request->only(['old_password', 'password']);

        $result = $this->userService->passwordReset($data, $user);

        if (!$result) {
            return redirect()->back()->withErrors(['password' => 'Неверный пароль']);
        }

        return redirect()->route('users.personal');
    }
}
