<?php

namespace App\Http\Controllers;

use App\Http\Requests\CallsStoreRequest;
use App\Services\CallsService;
use App\Services\Uis\UisApiService;
use Illuminate\Http\Request;

class CallsController extends Controller
{
    /**
     * @var UisApiService
     */
    private $uisApiService;
    /**
     * @var CallsService
     */
    private $callsService;


    /**
     * @param CallsService $callsService
     * @param UisApiService $uisApiService
     */
    public function __construct(CallsService $callsService, UisApiService $uisApiService)
    {
        $this->uisApiService = $uisApiService;
        $this->callsService = $callsService;
    }


    /**
     * Поиск пропущенных звонков в базе
     *
     * @param string $date
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(string $date = '0')
    {
        $callsPaginator = $this->callsService->getCallsByDate($date);

        return view('calls.index', compact('callsPaginator'));
    }

    /**
     *  Получение пропущенных звонков из апи и сохранение в базу
     *
     * @param Request $request
     * @return void
     */
    public function store(CallsStoreRequest $request)
    {
        $this->callsService->storeCallsByDate($request->get('date'));

        return redirect()->route('calls.index', ['date' => $request->get('date')]);
    }

    /**
     * Изменения статуса звонка
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateCallsStatus(Request $request)
    {
        $this->callsService
            ->changeStatusCallsByNumberAndDate(
                $request->get('phoneNumber'),
                $request->get('date'),
                $request->get('processed')
            );

        return redirect()->back();
    }
}
