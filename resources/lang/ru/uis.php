<?php
/**
 * Переводы причин окончания сессии звонка для сервиса uis
 */
return [
    'finish_reasons' => [
        'no_success_subscriber_call' => 'Не дозвонились до абонента',
        'no_success_operator_call' => 'Не дозвонились до сотрудника',
        'subscriber_disconnects' => 'Абонент разорвал соединение',
        'timeout' => 'Время дозвона истекло',
        'operator_not_responsible' => 'Сотрудник не отвечает',
        'no_operation' => 'Нет операции для обработки',
        'employee_status_do_not_disturb' => 'Сотрудник в статусе "Не беспокоить"',
        'sip_offline' => 'SIP-линия не зарегистрирована',
        "operator_disconnects" => "Сотрудник разорвал соединение"
    ]
];
