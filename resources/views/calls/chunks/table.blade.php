<div class="table-responsive">

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th scope="col">Кто звонил</th>
            <th scope="col">Куда звонил</th>
            <th scope="col">Когда звонил</th>
            <th scope="col">Откуда</th>
            <th scope="col">Причина потери звонка</th>
            <th scope="col">Действие</th>
        </tr>
        </thead>
        <tbody>
        @foreach($callsPaginator->items() as $call)
            @include('calls.chunks.table-row', compact('call'))
        @endforeach
        </tbody>
    </table>
    {{$callsPaginator->links()}}
</div>
