<tr class="{{$call->processed ? "table-success": ""}}">

    <td>{{$call->from_phone_number}}</td>
    <td>{{$call->to_phone_number}}</td>
    <td>{{$call->date_time}}</td>
    <td>
        {{$call->region_name}}
    </td>
    <td>@lang('uis.finish_reasons.' . $call->finish_reason)</td>
    <td>
        <form method="POST" action="{{route('calls.status.update')}}">
            @csrf
            @method("PUT")
            <input type="hidden" name="date" value="{{$call->date_time}}">
            <input type="hidden" name="phoneNumber" value="{{$call->from_phone_number}}">
            <input type="hidden" name="processed" value="{{(int)!$call->processed}}">

            @if(!$call->processed)
                <button class="btn btn-success">Звонок совершен</button>
            @else
                <button class="btn btn-secondary">Отменить</button>
            @endif

        </form>
    </td>
</tr>
