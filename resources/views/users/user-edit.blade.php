@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-header">
                        Создание пользователя
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="POST" action="{{route("users.update", ['id' => $user->id])}}">
                            @csrf
                            @method("PUT")
                            <div class="form-group">
                                <label for="name">Имя</label>
                                <input type="text"
                                       class="form-control"
                                       name="name" id="name"
                                       placeholder="name"
                                       value="{{$user->name}}"
                                >
                            </div>

                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="text"
                                       class="form-control"
                                       name="email" id="email"
                                       placeholder="email"
                                       value="{{$user->email}}"
                                >
                            </div>

                            <div class="form-group">
                                <label for="name">Пароль</label>
                                <input type="password"
                                       class="form-control"
                                       name="password" id="password"
                                       placeholder="password"
                                >
                            </div>
                            <button type="submit" class="btn btn-primary btn-block mt-4">Обновить</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
