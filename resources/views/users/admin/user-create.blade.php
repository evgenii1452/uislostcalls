@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-header">
                        Создание пользователя
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="POST" action="{{route('admin.users.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="name">Имя</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="name">
                            </div>

                            <div class="form-group">
                                <label for="login">Логин</label>
                                <input type="text" class="form-control" name="login" id="login" placeholder="login">
                            </div>

                            <div class="form-group">
                                <label for="password">Пароль</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="******">
                            </div>

                            <div class="form-group">
                                <label for="password_confirmation">Повторите пароль</label>
                                <input type="password" class="form-control" name="password_confirmation" id="confirmPassword" placeholder="******">
                            </div>

                            <button type="submit" class="btn btn-primary btn-block mt-4">Добавить</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
