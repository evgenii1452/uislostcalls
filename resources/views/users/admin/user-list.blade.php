@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Список пользователей
                    </div>
                    <div class="card-body">
                        <a href="{{route('admin.users.create')}}">
                            <button class="btn btn-primary mb-4">Создать</button>
                        </a>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Имя</th>
                                    <th scope="col">Логин</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Роль</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->login}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->isAdmin() ? "Админ" : "Пользователь"}}</td>
                                        <td>
                                            <form method="POST" action="{{route('admin.users.destroy', ['id' => $user->id])}}">
                                                @csrf
                                                @method("DELETE")
                                                <button class="btn btn-danger" onclick="return confirm('Вы уверены?')">Удалить</button>
                                            </form>
                                        </td>
                                        <td>
                                            <a href="{{route('admin.users.edit', ['id' => $user->id])}}">
                                                <button class="btn btn-primary">Изменить</button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
