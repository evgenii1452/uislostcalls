@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-header">
                        Личный кабинет
                    </div>
                    <div class="card-body">

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Имя: {{$user->name}}</li>
                            <li class="list-group-item">Логин: {{$user->login}}</li>
                            <li class="list-group-item">Email: {{$user->email}}</li>
                            <li class="list-group-item"><a href="{{route('users.password-reset')}}">Изменить пароль</a></li>
                            @if($user->isAdmin())
                                <li class="list-group-item"><a href="{{route('admin.users.index')}}">Список пользователей</a></li>
                                <li class="list-group-item"><a href="{{route('admin.users.create')}}">Добавить пользователя</a></li>
                            @endif
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
