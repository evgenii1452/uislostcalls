@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-header">
                        Изменение пароля
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="post" action="{{route('users.password-reset-handler')}}">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputPassword1">Старый пароль</label>
                                <input type="password" class="form-control" name="old_password" id="oldPassword" placeholder="******">
                            </div>

                            <div class="form-group">
                                <label for="password">Новый пароль</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="******">
                            </div>

                            <div class="form-group">
                                <label for="confirmPassword">Повторите новый пароль</label>
                                <input type="password" class="form-control" name="password_confirmation" id="confirmPassword" placeholder="******">
                            </div>

                            <button type="submit" class="btn btn-primary btn-block mt-4">Изменить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
